package main

import (
	"application/routes"
)

func main() {
	routes.InitializeRoutes()
}
