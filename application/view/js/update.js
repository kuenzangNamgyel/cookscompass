const urlParams = new URLSearchParams(window.location.search);
const recipeId = urlParams.get('id');

let recipeName = document.querySelector('.recipeName');
let description = document.querySelector('.description');
let ingredient = document.querySelector('.ingredient');
let instruction = document.querySelector('.instruction');

window.onload = () => {
    fetch(`/recipe/${recipeId}`).then(res => res.text()).then(data => {
        let recipe = JSON.parse(data)
        recipeName.value = recipe.recipeName
        description.value = recipe.description
        ingredient.value = recipe.ingredient
        instruction.value = recipe.instruction
    }).catch(e => e)
}

document.querySelector('.updateButton').addEventListener('click', function(event) {

    let recipe = {
        RecipeID:parseInt(recipeId),
        recipeName: document.querySelector('.recipeName').value,
        description: document.querySelector('.description').value,
        ingredient: document.querySelector('.ingredient').value,
        instruction: document.querySelector('.instruction').value,
        image: localStorage.getItem("image-dataurl"),
    };

    console.log(recipe);

    fetch(`/recipe/update/${parseInt(recipeId)}`,{
        method:"PUT",
        headers: {"Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(recipe)
    })
        .then(res => {
            if(res.ok){
                alert("Date updated")
                window.open("home.html","__self")
            }
        })
        .catch(e => console.log(e))
});

// document.getElementById('image').addEventListener('change', function(event) {
//     let imagePreview = document.getElementById('imagePreview');
//     imagePreview.src = URL.createObjectURL(event.target.files[0]);
// });

document.getElementById("image").addEventListener("change",function(){
    const dataurl = new FileReader();
    dataurl.addEventListener("load",() => {
        localStorage.setItem("image-dataurl",dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})

function redirectToRecipePage() {
    window.location.href = "recipe.html"; 
    window.history.back();
}