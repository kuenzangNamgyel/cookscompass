let form = document.querySelector('.recipeForm')

document.getElementById("image").addEventListener("change",function(){
    const dataurl = new FileReader();
    dataurl.addEventListener("load",() => {
        localStorage.setItem("image-dataurl",dataurl.result)
    })
    dataurl.readAsDataURL(this.files[0])
})

function Submit() {
    let recipe = {
        recipeName:document.querySelector('.recipeName').value,
        description:document.querySelector('.description').value,
        ingredient:document.querySelector('.ingredient').value,
        instruction:document.querySelector('.instruction').value,
        image:localStorage.getItem("image-dataurl")
    }

    console.log(recipe);

    fetch("/recipe/add", {
        method: "POST",
        headers: {"Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(recipe)
    })
    .then((res) => {
        if (res.ok){
            alert("Recipe Added")
            window.open("home.html","__self")
        }
    })
    .catch((e) => console.log(e))
}

// --------------------------------------------------------------

// var addImageBtn = document.getElementById("addImageBtn");
// addImageBtn.addEventListener("click", function() {
//   var imageInput = document.createElement("input");
//   imageInput.type = "file";
//   imageInput.accept = "image/*";
//   imageInput.addEventListener("change", function() {
//     var file = imageInput.files[0];
//     if (file) {
//       var reader = new FileReader();

//       reader.addEventListener("load", function() {
//         imagePreview.style.display = "block";
//         imagePreview.querySelector("img").setAttribute("src", reader.result);
//       });

//       reader.readAsDataURL(file);
//     } else {
//       imagePreview.style.display = "none";
//       imagePreview.querySelector("img").setAttribute("src", "");
//     }
//   });

//   imageInput.click();
// });

// var imagePreview = document.getElementById("imagePreview");
// var cancelBtn = document.getElementById("cancelBtn");
// cancelBtn.addEventListener("click", function() {
//   window.location.href = "index.html"; // Redirect to another page or perform desired action
// });