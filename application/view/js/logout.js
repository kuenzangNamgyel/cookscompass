function Logout() {
    fetch("/logout")
    .then(response => {
        if (response.ok){
            console.log("success");
            window.open("index.html","_self")
        }else{
            throw response.statusText
        }
    }).catch(e => {
        alert(e)
    })
}