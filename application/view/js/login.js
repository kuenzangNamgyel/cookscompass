var emailInput = document.getElementById('email');
var passwordInput = document.getElementById('password')
var form = document.getElementById("login-form")

    const validateForm = () => {
        let isEmailValid = checkEmail(),
            isPasswordValid = checkPassword();
        let isFormValid = isEmailValid && isPasswordValid;

        if (isFormValid){

            const data = {
                email: emailInput.value,
                password: passwordInput.value
            }

            fetch("/user/login",{
                method:"POST",
                body:JSON.stringify(data),
                headers: {"Content-type":"application/json; charset=UTF-8"}
            }).then((res) => {
                if (res.ok){
                    alert("success")
                    window.open("home.html", "_self");
                }else if(res.status === 404) {
                    alert("Invalid Credential");
                }else{
                    throw new Error(res.statusText)
                }
            }).catch((e) => {
                console.log(e);
            })
        }
    }
         
    var emailInput = document.getElementById('email');
    var passwordInput = document.getElementById('password');
    var emailError = document.getElementById('emailError');
    var passwordError = document.getElementById('passwordError');

    emailInput.addEventListener('input', function(){
        checkEmail()
    })
    passwordInput.addEventListener('input',function(){
        checkPassword();
    })

    const checkEmail = () => {
        if (!isRequired(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email cannot be blank.';
            return false;
        } else if (!isEmailValid(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email is not valid.';
            return false;
        } else{
            emailInput.classList.add('is-valid');
            emailInput.classList.remove('is-invalid');
            emailError.innerHTML = '';
            return true;
        }
    }
    const isEmailValid = (email) => {
        const re = /^[A-Za-z0-9._%+-]+@[a-z]+\.com$/;
        // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return re.test(email)
    }
    // const isRequired = value => value === ''? false : true;
    // const isRequired = value => value === '' || value === null || typeof value === 'undefined' ? false : true;
    const isRequired = value => value.trim() === ''? false : true;
    // const isRequired = value => !value;

    const checkPassword = () => {
        if (!isRequired(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password cannot be blank.';
            return false;
        } else if(!isPasswordValid(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password must have at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
            return false;
        } else {
            passwordInput.classList.add('is-valid');
            passwordInput.classList.remove('is-invalid');
            passwordError.innerHTML = '';
            return true;
        }
    }
    const isPasswordValid = (password) => {
        const re = new
        RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        return re.test(password);
    }