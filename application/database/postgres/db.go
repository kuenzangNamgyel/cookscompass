package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	postgres_host     = "db"
	postgres_port     = 5432
	postgres_user     = "postgres"
	postgres_password = "postgres"
	postgres_dbname   = "cookscompass"
)

// const (
// 	postgres_host     = "dpg-chqno067avjb90memb80-a.singapore-postgres.render.com"
// 	postgres_port     = 5432
// 	postgres_user     = "postgres_admin"
// 	postgres_password = "yW07zdrgT1SLzGNxZsRXt7uc1GhnS9pD"
// 	postgres_dbname   = "my_db_x5ld"
// )

var Db *sql.DB

func init() {
	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error

	Db, err = sql.Open("postgres", db_info)

	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}
